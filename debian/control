Source: rust-z-base-32
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Dylan Aïssi <daissi@debian.org>
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/z-base-32]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/z-base-32
X-Cargo-Crate: z-base-32
Rules-Requires-Root: no

Package: librust-z-base-32-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends}
Suggests:
 librust-z-base-32+pyo3-dev (= ${binary:Version})
Provides:
 librust-z-base-32+default-dev (= ${binary:Version}),
 librust-z-base-32-0-dev (= ${binary:Version}),
 librust-z-base-32-0+default-dev (= ${binary:Version}),
 librust-z-base-32-0.1-dev (= ${binary:Version}),
 librust-z-base-32-0.1+default-dev (= ${binary:Version}),
 librust-z-base-32-0.1.2-dev (= ${binary:Version}),
 librust-z-base-32-0.1.2+default-dev (= ${binary:Version})
Description: Z-base-32: human-oriented base-32 encoding - Rust source code
 This package contains the source for the Rust z-base-32 crate, packaged by
 debcargo for use with cargo and dh-cargo.

Package: librust-z-base-32+pyo3-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-z-base-32-dev (= ${binary:Version}),
 librust-pyo3+default-dev (>= 0.14.2-~~),
 librust-pyo3+extension-module-dev (>= 0.14.2-~~)
Provides:
 librust-z-base-32+python-dev (= ${binary:Version}),
 librust-z-base-32-0+pyo3-dev (= ${binary:Version}),
 librust-z-base-32-0+python-dev (= ${binary:Version}),
 librust-z-base-32-0.1+pyo3-dev (= ${binary:Version}),
 librust-z-base-32-0.1+python-dev (= ${binary:Version}),
 librust-z-base-32-0.1.2+pyo3-dev (= ${binary:Version}),
 librust-z-base-32-0.1.2+python-dev (= ${binary:Version})
Description: Z-base-32: human-oriented base-32 encoding - feature "pyo3" and 1 more
 This metapackage enables feature "pyo3" for the Rust z-base-32 crate, by
 pulling in any additional dependencies needed by that feature.
 .
 Additionally, this package also provides the "python" feature.
